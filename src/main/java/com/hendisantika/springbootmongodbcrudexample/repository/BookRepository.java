package com.hendisantika.springbootmongodbcrudexample.repository;

import com.hendisantika.springbootmongodbcrudexample.entity.Book;
import org.springframework.data.mongodb.repository.MongoRepository;


/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 04/05/21
 * Time: 23.36
 */
public interface BookRepository extends MongoRepository<Book, String> {
}
