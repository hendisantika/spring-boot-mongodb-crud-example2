package com.hendisantika.springbootmongodbcrudexample.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 05/05/21
 * Time: 14.40
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Book2")
public class Book2 {
    @Id
    private Integer id;
    private String name;
    private Integer pages;
    private String author;
    private Double cost;
}
