package com.hendisantika.springbootmongodbcrudexample.init;

import com.hendisantika.springbootmongodbcrudexample.entity.Book2;
import com.hendisantika.springbootmongodbcrudexample.repository.Book2Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-mongodb-crud-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/05/21
 * Time: 15.03
 */
@Component
public class SaveBooksRunner implements CommandLineRunner {

    @Autowired
    private Book2Repository bookRepo;

    @Override
    public void run(String... args) throws Exception {
        bookRepo.saveAll(List.of(
                new Book2(500, "Core Java", 200, "Kathy Sierra", 1065.5),
                new Book2(501, "JSP & Servlets", 350, "Kathy Sierra", 1749.0),
                new Book2(502, "Spring in Action", 480, "Craig Walls", 940.75),
                new Book2(503, "Pro Angular", 260, "Freeman", 1949.25),
                new Book2(504, "HTML CSS", 100, "Thomas Powell", 2317.09),
                new Book2(505, "Hibernate in Action", 180, "Gavin King", 889.25),
                new Book2(506, "Practical MongoDB", 180, "Shakuntala Gupta", 785.0),
                new Book2(507, "Pro Spring Boot", 850, "Felipe Gutierrez", 2167.99),
                new Book2(508, "Beginning jQuery", 180, "Franklin", 1500.00),
                new Book2(509, "Java Design Patterns", 114, "Devendra Singh", 919.99)
        ));

        System.out.println("------All records has been saved successfully-------");

    }
}

